import "./app.css";

export const metadata = {
  title: process.env.APPNAME,
  description: process.env.DESCRIPTION,
};

export default function RootLayout({ children }) {
  return (
    <html lang="en" className="dark">
      <body
        className={`font-sans antialiased dark:bg-slate-900 bg-white overflow-x-hidden selection:bg-transparent selection:text-blue-600`}
      >
        {children}
      </body>
    </html>
  );
}
