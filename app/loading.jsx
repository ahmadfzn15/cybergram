"use client";

import { Spinner } from "@material-tailwind/react";

export default function Loading() {
  return (
    <>
      <div className="inset-0 fixed flex justify-center items-center bg-transparent">
        <Spinner className="w-16 h-16" />
      </div>
    </>
  );
}
