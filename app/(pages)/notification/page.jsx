"use client";

import { List, ListItem } from "@material-tailwind/react";

export default function Notification() {
  return (
    <>
      <div className="space-y-10 p-10">
        <h1 className="text-2xl">Notification</h1>
        <List className="border text-slate-400 border-blue-500/50 rounded-lg">
          <ListItem className="focus:bg-transparent active:bg-transparent hover:bg-transparent hover:text-slate-300 active:text-slate-200 focus:text-slate-400 flex flex-col items-start gap-3">
            <h1 className="text-slate-200">Mode</h1>
            <p className="text-sm">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus
              commodi incidunt at earum autem eveniet eaque impedit ad aperiam
              repudiandae!
            </p>
          </ListItem>
          <ListItem className="focus:bg-transparent active:bg-transparent hover:bg-transparent hover:text-slate-300 active:text-slate-200 focus:text-slate-400 flex flex-col items-start gap-3 border-y border-blue-500/50 rounded-none">
            <h1 className="text-slate-200">Privasi</h1>
            <p className="text-sm">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus
              commodi incidunt at earum autem eveniet eaque impedit ad aperiam
              repudiandae!
            </p>
          </ListItem>
          <ListItem className="focus:bg-transparent active:bg-transparent hover:bg-transparent hover:text-slate-300 active:text-slate-200 focus:text-slate-400 flex flex-col items-start gap-3">
            <h1 className="text-slate-200">About</h1>
            <p className="text-sm">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus
              commodi incidunt at earum autem eveniet eaque impedit ad aperiam
              repudiandae!
            </p>
          </ListItem>
        </List>
      </div>
    </>
  );
}
