"use client";

import {
  Avatar,
  IconButton,
  Input,
  Menu,
  MenuHandler,
  MenuItem,
  MenuList,
} from "@material-tailwind/react";
import RightBar from "../../components/RightBar";
import { PaperAirplaneIcon, PhotoIcon } from "@heroicons/react/24/outline";
import { EllipsisVerticalIcon } from "@heroicons/react/24/solid";
import Link from "next/link";
import { useState } from "react";
import { auth, db } from "@/firebaseConfig";
import { addDoc, collection, serverTimestamp } from "firebase/firestore";

export const metadata = {
  title: "Chat",
  description: "Live Chat Room",
};

export default function Chat() {
  const [textMessage, setTextMessage] = useState("");
  const [loading, setLoading] = useState(false);

  const saveMessage = async () => {
    setLoading(true);
    alert(textMessage);
    try {
      await addDoc(collection(db, "messages"), {
        userID: auth.currentUser.uid,
        text: textMessage,
        timestamp: serverTimestamp(),
      });
    } catch (error) {
      console.error("Error writing new message to Firebase Database", error);
    }
    setLoading(false);
  };

  return (
    <>
      <div className="lg:max-w-[calc(100vw-35rem)] max-w-[calc(100vw-25rem)] portrait:max-w-[100vw] w-full bg-slate-900 h-16 fixed flex items-center justify-between p-5 z-40">
        <Link href="/lusi">
          <Avatar src="../../img/gelung.jpg" alt="gelung" withBorder />
        </Link>
        <Menu placement="bottom-end">
          <MenuHandler>
            <IconButton variant="text">
              <EllipsisVerticalIcon className="w-8 h-8" />
            </IconButton>
          </MenuHandler>
          <MenuList className="bg-slate-800 border-slate-500/50">
            <MenuItem className="">
              <h1>Hapus Chat</h1>
            </MenuItem>
          </MenuList>
        </Menu>
      </div>
      <div className="lg:max-w-[calc(100vw-35rem)] max-w-[calc(100vw-25rem)] portrait:max-w-[100vw] w-full flex flex-col pt-16">
        <div className="w-full space-y-5 p-5">
          <div className="flex items-start gap-3 float-left">
            <Avatar src="../../img/gelung.jpg" />
            <div className="rounded-e-3xl rounded-es-3xl p-4 border border-slate-500/50 bg-slate-800/80 max-w-[calc(100%-10rem)] flex flex-col items-start gap-4 float-left">
              <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
              <small>12.00</small>
            </div>
          </div>
          <div className="flex items-start gap-3 float-right">
            <Avatar src="../../img/gelung.jpg" />
            <div className="rounded-e-3xl rounded-es-3xl p-4 border border-slate-500/50 bg-slate-800/80 max-w-[calc(100%-10rem)] flex flex-col items-start gap-4 float-right">
              <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
              <small>12.00</small>
            </div>
          </div>
        </div>
        <div className="fixed bottom-0 w-[calc(100vw-35rem)] bg-slate-800 h-16 flex items-center gap-3 px-5 text-slate-300">
          <PhotoIcon className="w-10 h-10 cursor-pointer" />
          <Input
            variant="outlined"
            label="Type here"
            icon={
              <PaperAirplaneIcon
                onClick={saveMessage}
                className="w-6 h-6 cursor-pointer"
              />
            }
            value={textMessage}
            onChange={(e) => setTextMessage(e.target.value)}
            className="text-slate-300"
            onKeyUp={(e) => {
              if (e.keyCode == 13) {
                saveMessage();
              }
            }}
            autoFocus
          />
        </div>
      </div>
      <RightBar />
    </>
  );
}
