import AuthCheck from "../components/authCheck";
import Sidebar from "../components/sidebar";

export default function Template({ children }) {
  return (
    <AuthCheck>
      <div className="w-screen min-h-screen text-slate-300">
        <Sidebar />
        <main
          className={`max-w-[calc(100vw-5rem)] portrait:max-w-[100vw] lg:max-w-[calc(100vw-25rem)]} min-h-screen ml-20 lg:ml-60 portrait:ml-0`}
        >
          {children}
        </main>
      </div>
    </AuthCheck>
  );
}
