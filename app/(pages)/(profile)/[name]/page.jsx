"use client";

import {
  Button,
  Dialog,
  DialogBody,
  Input,
  Menu,
  MenuHandler,
  MenuList,
} from "@material-tailwind/react";
import { useEffect, useState } from "react";

export default function Profile({ params }) {
  const name = params.name;
  const [profile, setProfile] = useState(false);
  const [menuFollower, setMenuFollower] = useState(false);
  const [menuFollowing, setMenuFollowing] = useState(false);
  const openProfile = () => setProfile(!profile);

  const Trigger = {
    onMouseEnter: () => setMenuFollower(true),
    onMouseLeave: () => setMenuFollower(false),
  };

  const Trigger2 = {
    onMouseEnter: () => setMenuFollowing(true),
    onMouseLeave: () => setMenuFollowing(false),
  };

  return (
    <>
      <div className="max-w-[calc(100%-5rem)] w-full mx-auto">
        <div className="border-b border-slate-500/50 pb-10 pt-5">
          <div className="max-w-[calc(100%-20rem)] mx-auto flex justify-center gap-20">
            <div
              className="w-40 h-40 rounded-full overflow-hidden ring-1 ring-slate-600 shrink-0"
              onClick={openProfile}
            >
              <img src="/img/aish.jpeg" alt={name} title={name} />
            </div>
            <div className=" flex flex-col gap-4">
              <div className="flex justify-between items-start gap-4">
                <h1 className="text-2xl">{name}</h1>
                <Button
                  variant="gradient"
                  className="capitalize text-sm px-5 py-2"
                >
                  Follow
                </Button>
              </div>
              <div className="flex justify-center gap-4 capitalize">
                <Menu
                  dismiss={{
                    itemPress: false,
                  }}
                  open={menuFollower}
                >
                  <MenuHandler {...Trigger}>
                    <Button
                      variant="text"
                      className="capitalize whitespace-nowrap text-sm py-2"
                    >
                      0 followers
                    </Button>
                  </MenuHandler>
                  <MenuList
                    {...Trigger}
                    className="w-max bg-slate-800 border-slate-500/50"
                  >
                    <h1 className="text-center">Nothing Follower</h1>
                  </MenuList>
                </Menu>
                <Menu
                  dismiss={{
                    itemPress: false,
                  }}
                  open={menuFollowing}
                >
                  <MenuHandler {...Trigger2}>
                    <Button
                      variant="text"
                      className="capitalize whitespace-nowrap text-sm py-2"
                    >
                      0 following
                    </Button>
                  </MenuHandler>
                  <MenuList
                    {...Trigger2}
                    className="w-max bg-slate-800 border-slate-500/50"
                  >
                    <h1 className="text-center">Nothing Following</h1>
                  </MenuList>
                </Menu>
                <Button
                  variant="text"
                  className="capitalize whitespace-nowrap text-sm py-2"
                >
                  0 post
                </Button>
              </div>
              <div>
                <p className="text-sm">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Necessitatibus repellat velit iste ipsum modi earum atque,
                  deleniti quidem eaque eius.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Dialog
        open={profile}
        handler={openProfile}
        className="bg-transparent rounded-full overflow-hidden"
      >
        <DialogBody className="flex justify-center items-center">
          <div className="bg-transparent rounded-full overflow-hidden w-[20rem] h-[20rem]">
            <img src="/img/aish.jpeg" alt="aish" />
          </div>
        </DialogBody>
      </Dialog>
    </>
  );
}
