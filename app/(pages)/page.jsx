"use client";

import { auth, db } from "@/firebaseConfig";
import {
  ChatBubbleBottomCenterTextIcon,
  EllipsisVerticalIcon,
  HeartIcon,
  ShareIcon,
} from "@heroicons/react/24/outline";
import { Avatar, Carousel, IconButton } from "@material-tailwind/react";
import { collection, getDocs } from "firebase/firestore";
import { useEffect, useState } from "react";

export default function Welcome() {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const querySnapshot = await getDocs(collection(db, "posts"));
      const newData = querySnapshot.docs.map((doc) => doc.data());
      setData(newData);
    };

    fetchData();
  }, []);

  return (
    <>
      <div className="flex flex-col items-center gap-10">
        {data.map((d, i) => (
          <div
            className="p-5 rounded-lg border border-slate-700 w-[35rem] flex flex-col space-y-4"
            key={i}
          >
            <div className="flex justify-between items-center">
              <div className="flex items-start space-y-1 gap-5">
                <Avatar
                  src="img/gelung.jpg"
                  withBorder
                  className="w-14 h-14 shrink-0"
                  alt="gelung"
                />
                <div className="flex flex-col">
                  <h1>Lusi Kuraisin</h1>
                  <p className="text-xs text-slate-400">1 month ago</p>
                </div>
              </div>
              <IconButton variant="text">
                <EllipsisVerticalIcon className="w-7 h-7" />
              </IconButton>
            </div>
            <Carousel className="max-w-[37.5rem] w-full rounded-lg">
              {d.image.map((image, i) => (
                <div
                  key={i}
                  className="max-h-[40rem] flex justify-center items-center overflow-hidden"
                >
                  <img
                    src={image}
                    className="rounded-md"
                    width="100%"
                    height="100%"
                  />
                </div>
              ))}
            </Carousel>
            <div className="self-end space-x-1">
              <IconButton variant="text">
                <HeartIcon className="w-7 h-7 text-white" />
              </IconButton>
              <IconButton variant="text">
                <ChatBubbleBottomCenterTextIcon className="w-7 h-7 text-white" />
              </IconButton>
              <IconButton variant="text">
                <ShareIcon className="w-7 h-7 text-white" />
              </IconButton>
            </div>
            <div className="">
              <p className="text-sm">{d.description}</p>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}
