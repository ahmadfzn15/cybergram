import AuthCheck from "../components/authCheck";

export default function RootLayout({ children }) {
  return (
    <>
      <AuthCheck>
        <div className="w-screen h-screen flex justify-center items-center text-slate-300">
          {children}
        </div>
      </AuthCheck>
    </>
  );
}
