"use client";

import AuthCheck from "@/app/components/authCheck";
import { auth, google } from "@/firebaseConfig";
import { EyeIcon, EyeSlashIcon } from "@heroicons/react/24/outline";
import { Button, Input } from "@material-tailwind/react";
import { createUserWithEmailAndPassword, signInWithPopup } from "firebase/auth";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useRef, useState } from "react";

export default function Register() {
  const pwd = useRef();
  const [pass, setPass] = useState(true);
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const route = useRouter();

  const showPassword = () => {
    setPass(!pass);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await createUserWithEmailAndPassword(auth, email, password);
      route.push("/");
    } catch (error) {
      console.log("Error logging in: ", error);
    }
  };

  const googleProvider = () => {
    signInWithPopup(auth, google);
  };

  return (
    <>
      <AuthCheck link="/register">
        <div className="rounded-lg border border-slate-500/50 p-10 max-w-[30rem] w-full flex flex-col gap-5">
          <h1 className="text-center text-2xl">Sign up</h1>
          <form onSubmit={handleSubmit}>
            <div className="w-full flex flex-col gap-4">
              <Input
                label="Username"
                variant="outlined"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                required
              />
              <Input
                label="Email"
                variant="outlined"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
              <Input
                type={pass ? "password" : "text"}
                ref={pwd}
                label="Password"
                variant="outlined"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
                icon={
                  pass ? (
                    <EyeIcon className="w-6 h-6" onClick={showPassword} />
                  ) : (
                    <EyeSlashIcon className="w-6 h-6" onClick={showPassword} />
                  )
                }
              />
              <Button type="submit" variant="gradient" fullWidth>
                Sign up
              </Button>
              <Link href="/login">Already account?</Link>
              <Button onClick={googleProvider} variant="gradient" fullWidth>
                Sign up with Google
              </Button>
            </div>
          </form>
        </div>
      </AuthCheck>
    </>
  );
}
