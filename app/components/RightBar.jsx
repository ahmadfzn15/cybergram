"use client";

import { MagnifyingGlassIcon, PlusIcon } from "@heroicons/react/24/solid";
import { Avatar, IconButton, Input } from "@material-tailwind/react";

export default function RightBar() {
  return (
    <>
      <div className="fixed portrait:hidden right-0 top-0 w-[20rem] h-screen bg-slate-900 px-5 flex flex-col justify-between border-l border-blue before:contents[''] before:absolute before:right-20 before:bottom-0 before:w-32 before:h-36 before:bg-blue before:blur-[80px] before:-skew-x-[45deg]">
        <div className="flex flex-col items-start gap-">
          <div className="w-full h-16 flex items-center">
            <label htmlFor="search"></label>
            <Input
              id="search"
              icon={<MagnifyingGlassIcon className="w-5 h-5" />}
              label="Search Chat"
            />
          </div>
          <div className="space-y-2">
            <div className="rounded-lg bg-blue-900/50 p-2 w-full flex items-center gap-3">
              <Avatar
                src="../../img/gelung.jpg"
                alt="gelung"
                className="w-10 h-10"
              />
              <div className="">
                <h1>Ahmad</h1>
                <p className="text-sm text-white">
                  Lorem ipsum dolor sit amet...
                </p>
              </div>
            </div>
            <div className="rounded-lg bg-blue-900/50 p-2 w-full flex items-start gap-3">
              <Avatar
                src="../../img/gelung.jpg"
                alt="gelung"
                className="w-10 h-10"
              />
              <div className="">
                <h1>Lusi</h1>
                <p className="text-sm text-slate-400">
                  Lorem ipsum dolor sit amet...
                </p>
              </div>
            </div>
          </div>
        </div>
        <IconButton className="rounded-full self-end" variant="gradient">
          <PlusIcon className="w-10 h-10" />
        </IconButton>
      </div>
    </>
  );
}
