"use client";

import { useEffect } from "react";
import { useRouter } from "next/navigation";
import { auth } from "@/firebaseConfig";

export default function AuthCheck({ children, link }) {
  const router = useRouter();

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      if (user && router.pathname === link) {
        router.push("/");
      } else if (!user && router.pathname !== link) {
        router.push("/login");
      }
    });

    return () => unsubscribe();
  }, []);

  return children;
}
