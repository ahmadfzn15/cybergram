import { XMarkIcon } from "@heroicons/react/24/solid";
import {
  Button,
  Dialog,
  DialogBody,
  DialogHeader,
  IconButton,
  Input,
  Textarea,
} from "@material-tailwind/react";
import { useState } from "react";
import { collection, addDoc, serverTimestamp } from "firebase/firestore";
import {
  getStorage,
  ref,
  getDownloadURL,
  uploadBytesResumable,
} from "firebase/storage";
import { auth, db } from "@/firebaseConfig";

export default function AddPost({ post, openPost }) {
  const [image, setImage] = useState([]);
  const [description, setDescription] = useState("");
  const [uploadProgress, setUploadProgress] = useState(0);

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const storage = getStorage();
      const imageUrls = [];

      for (const img of image) {
        const storageRef = ref(storage, `post-images/${img.name}`);
        const uploadTask = uploadBytesResumable(storageRef, img);

        uploadTask.on(
          "state_changed",
          (snapshot) => {
            const progress =
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            setUploadProgress(progress);
          },
          (error) => {
            console.error("Gagal mengunggah gambar:", error);
          }
        );
        await uploadTask;
        // Dapatkan URL unduhan gambar yang diunggah
        const downloadURL = await getDownloadURL(storageRef);
        imageUrls.push(downloadURL);
      }

      await addDoc(collection(db, "posts"), {
        userID: auth.currentUser.uid,
        image: imageUrls,
        description: description,
        timestamps: serverTimestamp(),
      });

      // Reset form setelah berhasil menyimpan data
      setDescription("");
      setImage([]);
      setUploadProgress(0);
      post = false;

      console.log("Data postingan berhasil disimpan");
    } catch (error) {
      console.error("Gagal menyimpan data postingan:", error);
    }
  };

  return (
    <>
      <Dialog open={post} handler={openPost} className="bg-slate-900 px-3">
        <DialogHeader className="flex flex-col items-center gap-5">
          <IconButton variant="text" onClick={openPost}>
            <XMarkIcon className="w-10 h-10" />
          </IconButton>
        </DialogHeader>
        <DialogBody>
          <form onSubmit={handleSubmit}>
            <div className="w-full flex flex-col gap-5 items-center rounded-lg">
              <Input
                type="file"
                label="Image"
                onChange={(e) => setImage(Array.from(e.target.files))}
                required
                className="file:bg-transparent file:border file:border-slate-500/50 file:rounded-lg file:text-slate-300"
                multiple
              />
              <Textarea
                type="text"
                label="Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
              <Button type="submit" variant="gradient" className="self-end">
                Upload Post
              </Button>
            </div>
          </form>
        </DialogBody>
      </Dialog>
    </>
  );
}
