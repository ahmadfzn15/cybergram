"use client";

import {
  ArrowRightOnRectangleIcon,
  BellIcon,
  ChatBubbleOvalLeftEllipsisIcon,
  Cog6ToothIcon,
  HomeIcon,
  MagnifyingGlassIcon,
  PlusIcon,
} from "@heroicons/react/24/solid";
import Link from "next/link";
import { useState } from "react";
import Search from "./search";
import AddPost from "./addPost";
import { usePathname } from "next/navigation";
import { List, ListItem } from "@material-tailwind/react";
import { signOut } from "firebase/auth";
import { auth } from "@/firebaseConfig";

export default function Sidebar() {
  const [sidebar, setSidebar] = useState(true);
  const [search, setSearch] = useState(false);
  const [post, setPost] = useState(false);

  const openSearch = () => setSearch(!search);
  const openPost = () => setPost(!post);
  const path = usePathname();

  const logout = () => {
    try {
      signOut(auth);
    } catch (error) {
      console.log(error);
    }
  };

  const menu = [
    {
      link: "/",
      label: "Home",
      icon: <HomeIcon className="w-6 h-6 z-[2]" />,
    },
    {
      label: "Search",
      onclick: openSearch,
      icon: <MagnifyingGlassIcon className="w-6 h-6 z-[2]" />,
    },
    {
      link: "/chat",
      label: "Chat",
      icon: <ChatBubbleOvalLeftEllipsisIcon className="w-6 h-6 z-[2]" />,
    },
    {
      link: "/notification",
      label: "Notification",
      icon: <BellIcon className="w-6 h-6 z-[2]" />,
    },
    {
      label: "Create",
      onclick: openPost,
      icon: <PlusIcon className="w-6 h-6 z-[2]" />,
    },
    {
      label: "Logout",
      onclick: logout,
      icon: <ArrowRightOnRectangleIcon className="w-6 h-6 z-[2]" />,
    },
    {
      link: "/settings",
      label: "Settings",
      icon: <Cog6ToothIcon className="w-6 h-6 z-[2]" />,
    },
  ];

  return (
    <>
      <div
        className={`${
          sidebar
            ? "max-w-[5rem] lg:max-w-[15rem] portrait:max-w-[100vw]"
            : "lg:max-w-[5rem]"
        } h-screen portrait:h-20 w-full fixed left-0 portrait:bottom-0 border-right dark:border-right-dark transition-all ease-in dark:bg-slate-900 z-40`}
      >
        <div
          className={`w-full h-full p-5 flex landscape:flex-col gap-10 items-center relative before:contents[''] before:absolute before:left-20 before:w-32 before:h-36 before:bg-blue before:blur-[80px] before:-skew-x-[45deg] after:contents[''] after:absolute after:bottom-0 after:left-40 after:w-32 after:h-20 after:bg-blue after:blur-[70px]`}
        >
          <Link
            href="/"
            className="relative actives hover:before:w-full portrait:hidden"
          >
            {sidebar ? (
              <img
                src="../../img/judul.png"
                alt="Social Media"
                className="w-40 portrait:hidden"
                width="100%"
                height="100%"
              />
            ) : (
              ""
            )}
          </Link>
          <div className="w-full h-full flex portrait:justify-evenly landscape:flex-col portrait:items-center gap-5">
            {menu.map((d, i) =>
              d.link && d.link ? (
                <Link href={d.link && d.link} key={i}>
                  <div
                    className={`relative sidebar hover:scale-105 duration-300 hover:before:w-[calc(100%+30px)] ${
                      path == d.link && "before:w-[calc(100%+30px)]"
                    } overflow-hidden flex items-center space-x-5 p-2 portrait:p-4 rounded-lg select-none dark:text-slate-400 text-blue-800 hover:text-slate-100`}
                  >
                    {d.icon}
                    {sidebar ? (
                      <h1 className="z-[2] hidden lg:block">{d.label}</h1>
                    ) : (
                      ""
                    )}
                  </div>
                </Link>
              ) : (
                <div
                  onClick={d.onclick}
                  key={i}
                  className={`relative sidebar hover:scale-105 duration-300 hover:before:w-[calc(100%+30px)] overflow-hidden flex items-center space-x-5 p-2 portrait:p-4 rounded-lg select-none dark:text-slate-400 text-blue-800 hover:text-slate-100 cursor-pointer`}
                >
                  {d.icon}
                  {sidebar ? (
                    <h1 className="z-[2] hidden lg:block">{d.label}</h1>
                  ) : (
                    ""
                  )}
                </div>
              )
            )}
          </div>
        </div>
      </div>

      <Search
        isOpen={search}
        onClose={() => setSearch(!search)}
        handler={openSearch}
      />

      <AddPost post={post} openPost={openPost} />
    </>
  );
}
