"use client";

import { auth, db } from "@/firebaseConfig";
import { XMarkIcon } from "@heroicons/react/24/outline";
import { MagnifyingGlassIcon } from "@heroicons/react/24/solid";
import {
  Dialog,
  DialogBody,
  DialogHeader,
  IconButton,
  Input,
} from "@material-tailwind/react";
import { collection, query } from "firebase/firestore";
import { useEffect, useRef, useState } from "react";

export default function Search({ isOpen, onClose, handler }) {
  const refSearch = useRef(null);
  const result = useRef(null);
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async (e) => {
    if (searchQuery != "") {
      const user = auth.listUsers();
      console.log(user);
      result.current &&
        (result.current.innerHTML = `Search result '${searchQuery}'`);
    } else {
      result.current && (result.current.innerHTML = "Not found anything");
    }
  };

  useEffect(() => {
    handleSearch();
  }, [searchQuery]);

  return (
    <>
      <Dialog
        open={isOpen}
        onClose={onClose}
        handler={handler}
        className="bg-slate-900 px-3"
        size="lg"
      >
        <DialogHeader className="flex flex-col items-center gap-5">
          <IconButton variant="text" onClick={onClose}>
            <XMarkIcon className="w-10 h-10" />
          </IconButton>
          <Input
            icon={<MagnifyingGlassIcon className="w-5 h-5" />}
            ref={refSearch}
            variant="outlined"
            label="Search"
            name="search"
            className="text-slate-300"
            value={searchQuery}
            onKeyUp={handleSearch}
            onChange={(e) => setSearchQuery(e.target.value)}
          />
        </DialogHeader>
        <DialogBody>
          {searchResults.length === 0 ? (
            <h1 className="text-center" ref={result}>
              Not found anything
            </h1>
          ) : (
            <div className="flex flex-col gap-2">
              {searchResults.map((user, i) => (
                <Link href="/" key={i}>
                  <div className="p-2 border border-slate-500/50 rounded-lg flex items-center gap-3">
                    <Avatar src="/img/aish.jpeg" />
                    <h1>{user.displayName}</h1>
                  </div>
                </Link>
              ))}
            </div>
          )}
        </DialogBody>
      </Dialog>
    </>
  );
}
