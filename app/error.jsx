"use client";

import { useEffect } from "react";

export default function Error({ error, reset }) {
  useEffect(() => {
    console.error(error);
  }, [error]);
  return (
    <>
      <div className="w-screen h-screen flex justify-center items-center">
        <h1 className="text-slate-300 text-4xl">Ada Yang Error Guys</h1>
      </div>
    </>
  );
}
